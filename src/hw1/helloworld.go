package main

import "fmt"

// Exercise #2
func invert(slice []int) []int {
	size := len(slice)
	res := make([]int, size)
	for i := 0; i < size; i++ {
		res[i] = slice[(size-1)-i]
	}
	return res
}

// Exercise #3
func isAnagram(stringOne string, stringTwo string) bool {
	if len(stringOne) != len(stringTwo) {
		return false
	}
	size := len(stringOne)
	for i := 0; i < size; i++ {
		if stringOne[i] != stringTwo[(size-1)-i] {
			return false
		}
	}
	return true
}

// Exercise #4
func appendString(s []string, val string) []string {

	var newCapacity int
	var res []string
	sizeS, capacityS := len(s), cap(s)
	if capacityS >= sizeS+1 {
		res = make([]string, sizeS+1, capacityS)
	} else {
		if capacityS == 0 {
			newCapacity = 1
		} else {
			newCapacity = capacityS * 2
		}
		res = make([]string, sizeS+1, newCapacity)
	}
	copy(res, s)
	res[sizeS] = val
	return res
}

func printStat(val []string) {
	fmt.Println("value: ", val, " | cap: ", cap(val), "len: ", len(val))
}

func main() {
	fmt.Println("Exercise #1")
	fmt.Println("Hello, 世界")

	fmt.Println("Exercise #2")
	var originalSlice []int = []int{1, 2, 3, 4, 5, 6, 7, 8}
	fmt.Println("Original slice: ", originalSlice)
	fmt.Println("Inverted slice: ", invert(originalSlice))

	fmt.Println("Exercise #3")
	fmt.Println("Is 'abc' anagram of 'bca': ", isAnagram("abc", "cba"))
	fmt.Println("Is 'xyz' anagram of 'zcy': ", isAnagram("xyz", "zcy"))
	fmt.Println("Is 'foo' anagram of 'oof': ", isAnagram("foo", "oof"))

	fmt.Println("Exercise #4")
	names := []string{}
	// printStat(names)
	// names = appendString(names, "Bob") // cap=1, len=1
	// printStat(names)
	// names = appendString(names, "Charlie") // cap=2, len=2
	// printStat(names)
	// names = appendString(names, "Jhon") // cap=4, len=3
	// printStat(names)
	// names = appendString(names, "Roger") // cap=4, len=4
	// printStat(names)
	// names = appendString(names, "Ric") // cap=8, len=5
	// printStat(names)
	for i := 0; i < 10; i++ {
		printStat(names)
		names = appendString(names, "Bob")
	}
}
